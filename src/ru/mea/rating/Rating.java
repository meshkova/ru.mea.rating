package ru.mea.rating;

public class Rating {
    public static void main(String[] args) {
        int[] mark = {5, 4, 3, 2, 2, 5, 5};
        double s = 0;
        for (int value : mark) {
            s = s + value;

        }
        s = s / mark.length;
        System.out.println("Средний балл равен " + s);
    }
}
