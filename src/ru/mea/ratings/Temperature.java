package ru.mea.ratings;

import java.util.Scanner;

import static java.lang.System.*;


public class Temperature {
    private static Scanner scanner = new Scanner(in);

    public static void main(String[] args) {
        int numberOfDays;
        out.print("Введите количество дней:");
        numberOfDays = scanner.nextInt();
        double[] temperature = new double[numberOfDays];
        /**
         * вызывает метод и говорит о то, что температура должна
         */
        fillTemperature(temperature);
        System.out.print("Средняя температура");
        System.out.printf("%5.1f\n", averageTemperature(temperature));

        System.out.print("Максимальная температура");
        System.out.printf("%5.1f\n", maxT(temperature));
        System.out.print("Минимальная температура");
        System.out.printf("%5.1f\n", minT(temperature));

        int days[] = new int[numberOfDays];
        System.out.print(" теплые дни ");
        outArray(days);
        System.out.print(" холодные дни ");
        outArrays(days);


    }

    /**
     * length позволяет плучить количество элементов в массиве
     *
     * @param temperature рандомно заполняемый массив
     */
    private static void fillTemperature(double[] temperature) {
        for (int i = 0; i < temperature.length; i++) {
            temperature[i] = -20 + (Math.random() * 50);
            out.printf("%1.1f ", temperature[i]);
        }
    }

    /**
     * Складывает всё количество градусов и длит на количество дней
     *
     * @param temperature от сюда берутся данные
     * @return возвращает сумму температур деленноую на количество дней
     */
    private static double averageTemperature(double[] temperature) {
        double sum = 0.0;
        for (double temperatureOfDay : temperature) {
            sum = sum + temperatureOfDay;
        }

        return sum / temperature.length;
    }

    public static double maxT(double[] temperature) {
        int i;
        double max = temperature[0];

        for (i = 0; i < temperature.length; i++) {
            if (max < temperature[i]) {
                max = temperature[i];
            }

        }
        return max;

    }

    public static double minT(double[] temperature) {
        int i;
        double min = temperature[0];

        for (i = 0; i < temperature.length; i++) {
            if (min > temperature[i]) {
                min = temperature[i];
            }

        }
        return min;

    }

    private static int[] warmDays(double temperature[], int numberOfDays, double max) {
        int days[] = new int[numberOfDays];
        int j = -1;
        for (int i = 0; i < temperature.length; i++) {
            if (temperature[i] == i + 1) {
                days[j] = i + 1;
                j++;

            }
        }
        return days;

    }

    private static int[] coldDays(double temperature[], int numberOfDays) {
        int days[] = new int[numberOfDays];
        int j = -1;
        for (int i = 0; i > temperature.length; i++) {
            if (temperature[i] == i + 1) {
                days[j] = i + 1;
                j++;

            }
        }
        return days;
    }

    private static void outArrays(int[] days) {
        for (int i = 0; i < days.length; i++) {

            System.out.printf(days[i] + "");
        }
    }


    private static void outArray(int[] days) {
        for (int i = 0; i < days.length; i++) {

            System.out.printf(days[i] + "");
        }
    }


}